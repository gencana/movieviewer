package com.gen.movieviewer.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import com.gen.movieviewer.R;

/**
 * Created by gencana on 07/04/2018.
 */

public class SquareBox extends AppCompatCheckBox {

    private boolean isActivated;

    public SquareBox(Context context) {
        super(context);
        init(null);
    }

    public SquareBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public SquareBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        if (attrs != null){
            proccessAttributes(attrs);
            setActivated(isActivated);
        }

        setButtonDrawable(null);
        setBackground(getResources().getDrawable(R.drawable.checkbox_state_list));
    }

    private void proccessAttributes(AttributeSet attrs) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
                attrs, R.styleable.SquareBox, 0, 0);
        try{
            isActivated = typedArray.getBoolean(R.styleable.SquareBox_isActivated, false);
        }finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }

    @Override
    public void setActivated(boolean activated) {
        this.isActivated = activated;
        super.setActivated(activated);
        if (activated){
            setClickable(false);
        }
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
    }
}
