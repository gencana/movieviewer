package com.gen.movieviewer.views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gen.movieviewer.R;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.utils.FormatUtil;
import com.otaliastudios.zoom.ZoomLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gencana on 09/04/2018.
 */

public class SeatMapView extends RelativeLayout implements SeatRowView.SelectedSeatCounter, SeatRowView.SeatSelectedListener{

    public interface SeatListener {

        void onMaxSeatSelected();

        void onEmptyMovie();
    }

    public static final int MAX_SELECTED_SEATS = 10;

    @BindView(R.id.layout_seat_map) LinearLayout layoutSeatMap;

    @BindView(R.id.btn_seat1) AppCompatButton btnSeat1;

    @BindView(R.id.btn_seat2) AppCompatButton btnSeat2;

    @BindView(R.id.btn_seat3) AppCompatButton btnSeat3;

    @BindView(R.id.btn_seat4) AppCompatButton btnSeat4;

    @BindView(R.id.btn_seat5) AppCompatButton btnSeat5;

    @BindView(R.id.btn_seat6) AppCompatButton btnSeat6;

    @BindView(R.id.btn_seat7) AppCompatButton btnSeat7;

    @BindView(R.id.btn_seat8) AppCompatButton btnSeat8;

    @BindView(R.id.btn_seat9) AppCompatButton btnSeat9;

    @BindView(R.id.btn_seat10) AppCompatButton btnSeat10;

    @BindView(R.id.txt_total_price) TextView txtTotalPrice;

    @BindView(R.id.zoom_layout) ZoomLayout zoomLayout;

    private List<String> selectedSeats;

    private AppCompatButton[] selectedSeatsArray;

    private SeatListener seatListener;

    private double price;

    private boolean hasSelectedMovie;

    private List<SeatRowView> rowViewList;

    public SeatMapView(Context context) {
        super(context);
        init();
    }

    public SeatMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SeatMapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_seat_map, this, true);
        ButterKnife.bind(view);
        setupView();
    }


    private void setupView() {
        selectedSeatsArray = new AppCompatButton[]{
                btnSeat1, btnSeat2, btnSeat3, btnSeat4, btnSeat5,
                btnSeat6, btnSeat7, btnSeat8, btnSeat9, btnSeat10
        };
        rowViewList = new ArrayList<>();
        selectedSeats = new ArrayList<>();

        txtTotalPrice.setText(FormatUtil.formatNumberWithCurrency(getContext(), 0));
    }


    @Override
    public int getTotalSelectedSeats() {
        return selectedSeats.size();
    }

    public void setHasSelectedMovie(boolean hasSelectedMovie) {
        this.hasSelectedMovie = hasSelectedMovie;
    }

    public void clearAllSelection(){
        clearRows();
        zoomLayout.getEngine().zoomTo(1f, true);
        selectedSeats.clear();
        updateSeats();
    }

    private void clearRows() {
        for (SeatRowView seatRowView : rowViewList){
            seatRowView.resetState();
        }
    }

    @Override
    public boolean hasSelectedMovie() {
        return hasSelectedMovie;
    }

    @Override
    public void handleMaxSelectedSeats() {
        if (seatListener != null){
            seatListener.onMaxSeatSelected();
        }
    }

    @Override
    public void handleNoMovieSelection() {
        if (seatListener != null){
            seatListener.onEmptyMovie();
        }
    }

    @Override
    public void onSeatClicked(String seatName, boolean isSelected) {
        if (isSelected){
            selectedSeats.add(seatName);
            addSelectedSeats(seatName);
        }
        else{
            if (selectedSeats.size() <= 0){
                return;
            }
            selectedSeats.remove(seatName);
            removeSelectedSeats();
        }

        updatePrice();
    }

    private void updatePrice() {
        txtTotalPrice.setText(FormatUtil.formatNumberWithCurrency(
                getContext(), price * selectedSeats.size()));
    }

    public void loadSeatMap(List<List<Seat>> seatRowsList) {
        if (seatRowsList == null) {
            return;
        }

        for(int i = 0; i < seatRowsList.size(); i++){
            List<Seat> row = seatRowsList.get(i);
            SeatRowView seatRow = new SeatRowView(getContext());
            seatRow.setSeatSelectedListener(this);
            seatRow.setSelectedSeatCounter(this);
            seatRow.addRow(row, i);
            rowViewList.add(seatRow);
            layoutSeatMap.addView(seatRow);
        }
    }

    public void setSeatListener(SeatListener seatListener) {
        this.seatListener = seatListener;
    }

    public void setPrice(double price) {
        this.price = price;
        updatePrice();
    }

    private void addSelectedSeats(String seatName){
        if (selectedSeats.size() <= MAX_SELECTED_SEATS){
            AppCompatButton btnSeat = selectedSeatsArray[selectedSeats.size() - 1];
            btnSeat.setVisibility(View.VISIBLE);
            btnSeat.setText(seatName);
        }
    }


    private void removeSelectedSeats(){
        int position = Math.max(0, selectedSeats.size() - 1);
        selectedSeatsArray[position].setVisibility(position == 0 || position == 5 ? View.INVISIBLE : View.GONE);
        updateSeats();
    }

    private void updateSeats() {
        for (int i = 0; i < selectedSeatsArray.length; i++){
            AppCompatButton btnSeat = selectedSeatsArray[i];
            if (i >= selectedSeats.size()){
                btnSeat.setVisibility(i == 0 || i == 5 ? View.INVISIBLE : View.GONE);
            }
            else{
                btnSeat.setVisibility(View.VISIBLE);
                btnSeat.setText(selectedSeats.get(i));
            }
        }

    }

}
