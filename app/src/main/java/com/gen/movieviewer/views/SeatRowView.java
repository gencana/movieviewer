package com.gen.movieviewer.views;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.gen.movieviewer.R;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.utils.ViewGenerator;
import com.gen.movieviewer.utils.ViewUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatRowView extends LinearLayout implements CompoundButton.OnCheckedChangeListener, View.OnTouchListener {

    public interface SelectedSeatCounter {

        int getTotalSelectedSeats();

        boolean hasSelectedMovie();

        void handleMaxSelectedSeats();

        void handleNoMovieSelection();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (selectedSeats != null &&
                (!selectedSeats.hasSelectedMovie() ||
                selectedSeats.getTotalSelectedSeats() >= SeatMapView.MAX_SELECTED_SEATS)){
            if (!((CheckBox)v).isChecked()){
                if (!selectedSeats.hasSelectedMovie()){
                    selectedSeats.handleNoMovieSelection();
                }
                else{
                    selectedSeats.handleMaxSelectedSeats();
                }

                return true;
            }
        }

        return false;
    }

    public interface SeatSelectedListener {

        void onSeatClicked(String seatName, boolean isSelected);
    }


    @BindView(R.id.layout) LinearLayout layoutView;

    private List<Seat> seatList;

    private SeatSelectedListener seatSelectedListener;

    private SelectedSeatCounter selectedSeats;

    private List<SquareBox> squareBoxList;

    public SeatRowView(Context context) {
        super(context);
        init();
    }

    public SeatRowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SeatRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.item_seat_row, this, true);
        ButterKnife.bind(view);
        squareBoxList = new ArrayList<>();
    }

    public void addRow(List<Seat> seatList, int position){
        this.seatList = seatList;
        if (seatList == null || seatList.size() == 0){
            return;
        }

        int screenSize = ViewUtil.getScreenWidth((Activity)getContext());
        int viewSize = screenSize/(seatList.size() + 4);
        for(Seat seat : seatList){
            SquareBox squareBox = ViewGenerator.addSeatView(getContext(),
                    seat, viewSize, this, this);
            squareBoxList.add(squareBox);
            layoutView.addView(squareBox);
        }

        String rowName = seatList.get(0).getRow();
        layoutView.addView(ViewGenerator.addRowLabel(getContext(), rowName, viewSize * 2));
        layoutView.addView(ViewGenerator.addRowLabel(getContext(), rowName, viewSize * 2), 0);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        seatSelectedListener.onSeatClicked((String)buttonView.getTag(), isChecked);
    }

    public void setSeatSelectedListener(SeatSelectedListener seatSelectedListener){
        this.seatSelectedListener = seatSelectedListener;
    }

    public void setSelectedSeatCounter(SelectedSeatCounter selectedSeats) {
        this.selectedSeats = selectedSeats;
    }

    public void resetState(){
        for (SquareBox squareBox : squareBoxList){
            if (squareBox.isChecked()){
                squareBox.setChecked(false);
            }
        }
    }
}
