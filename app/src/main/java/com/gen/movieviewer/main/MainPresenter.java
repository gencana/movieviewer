package com.gen.movieviewer.main;

import com.gen.movieviewer.api.response.MovieResponse;
import com.gen.movieviewer.data.interactor.MainInteractor;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by gencana on 07/04/2018.
 */

public class MainPresenter implements MainContract.Presenter{

    private MainContract.View view;

    private MainInteractor interactor;

    @Inject
    public MainPresenter(MainContract.View view, MainInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void setView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void getMovieDetails() {
        interactor.execute(new DisposableObserver<MovieResponse>() {
            @Override
            public void onNext(MovieResponse movieResponse) {
                if (movieResponse != null){
                    view.loadMoviewDetails(movieResponse);
                }
                else{
                    view.showError(null);
                }
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });

    }

    @Override
    public void onDestroy() {
        interactor.dispose();
    }
}
