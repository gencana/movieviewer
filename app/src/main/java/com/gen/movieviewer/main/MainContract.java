package com.gen.movieviewer.main;

import com.gen.movieviewer.api.response.MovieResponse;
import com.gen.movieviewer.base.BasePresenter;
import com.gen.movieviewer.base.BaseView;
import com.gen.movieviewer.model.Seat;

import java.util.List;

/**
 * Created by gencana on 07/04/2018.
 */

public interface MainContract {

    interface View extends BaseView{

        void loadMoviewDetails(MovieResponse movieResponse);
    }


    interface Presenter extends BasePresenter<View>{

        void getMovieDetails();
    }
}
