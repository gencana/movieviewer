package com.gen.movieviewer.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gen.movieviewer.R;
import com.gen.movieviewer.api.response.MovieResponse;
import com.gen.movieviewer.base.BaseActivity;
import com.gen.movieviewer.base.DaggerBaseActivity;
import com.gen.movieviewer.seatbooking.SeatBookingActivity;
import com.gen.movieviewer.utils.FormatUtil;
import com.gen.movieviewer.utils.ValidationUtil;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjection;

public class MainActivity extends BaseActivity implements MainContract.View{

    @BindView(R.id.layout_main) RelativeLayout layoutMain;

    @BindView(R.id.img_landscape) ImageView imgLandscape;

    @BindView(R.id.img_portrait) ImageView imgPortrait;

    @BindView(R.id.txt_name) TextView txtName;

    @BindView(R.id.txt_genre) TextView txtGenre;

    @BindView(R.id.txt_advisory_rating) TextView txtAdvisoryRating;

    @BindView(R.id.txt_duration) TextView txtDuration;

    @BindView(R.id.txt_release_date) TextView txtReleaseDate;

    @BindView(R.id.txt_sypnosis) TextView txtSypnosis;

    @BindView(R.id.txt_casts) TextView txtCasts;

    @BindView(R.id.layout_content) RelativeLayout layoutContent;

    private Snackbar snackbar;

    @Inject
    MainPresenter presenter;

    private String theaterName = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        showProgressBar(true);
        presenter.getMovieDetails();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.btn_view_seat_map)
    void onViewSeatMapClicked(){
        SeatBookingActivity.startActivity(this, theaterName);
    }

    @Override
    public void showError(String message) {
        showContentView(false);
        showProgressBar(false);
        showSnackBar(getString(R.string.error_default_message));
        showErrorPlaceholder(true);
    }

    @Override
    public void loadMoviewDetails(MovieResponse movieResponse) {
        showProgressBar(false);
        showErrorPlaceholder(false);
        showContentView(true);

        Glide.with(this)
                .load(movieResponse.getPosterLandscape())
                .into(imgLandscape);

        Glide.with(this)
                .load(movieResponse.getPoster())
                .into(imgPortrait);
        theaterName = movieResponse.getTheater();
        txtName.setText(ValidationUtil.validateNullInput(movieResponse.getCanonicalTitle()));
        txtGenre.setText(ValidationUtil.validateNullInput(movieResponse.getGenre()));
        txtAdvisoryRating.setText(ValidationUtil.validateNullInput(movieResponse.getAdvisoryRating()));
        txtDuration.setText(ValidationUtil.validateNullInput(FormatUtil.formatDuration(movieResponse.getRuntimeMinutes())));
        txtReleaseDate.setText(ValidationUtil.validateNullInput(FormatUtil.formatDate(movieResponse.getReleaseDate())));
        txtSypnosis.setText(ValidationUtil.validateNullInput(movieResponse.getSynopsis()));
        txtCasts.setText(ValidationUtil.validateNullInput(StringUtils.join(movieResponse.getCast(), ", ")));
    }


    private void showSnackBar(String message){
        snackbar = Snackbar.make(layoutMain, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    protected void retry() {
        presenter.getMovieDetails();
    }

    @Override
    public void showContentView(boolean show) {
        layoutContent.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
