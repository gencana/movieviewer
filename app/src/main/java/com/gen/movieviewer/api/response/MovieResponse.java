package com.gen.movieviewer.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gencana on 09/04/2018.
 */

public class MovieResponse implements ApiResponse{

    @SerializedName("movie_id")
    private String movieId;

    @SerializedName("advisory_rating")
    private String advisoryRating;

    @SerializedName("canonical_title")
    private String canonicalTitle;

    private List<String> cast;

    private String genre;

    @SerializedName("has_schedules")
    private int hasSchedules;

    @SerializedName("is_inactive")
    private int isInactive;

    @SerializedName("is_showing")
    private int isShowing;

    @SerializedName("link_name")
    private String linkName;

    private String poster;

    @SerializedName("poster_landscape")
    private String posterLandscape;

//    private List<String> ratings;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("runtime_mins")
    private long runtimeMinutes;

    private String synopsis;

    private String trailer;

    @SerializedName("average_rating")
    private String averageRating;

    @SerializedName("total_reviews")
    private int totalReviews;

    private List<String> variants;

    private String theater;

    private  int order;

    @SerializedName("is_featured")
    private int isFeatured;

    @SerializedName("watch_list")
    private boolean isWatchList;

    @SerializedName("your_rating")
    private int yourRating;

    public String getMovieId() {
        return movieId;
    }

    public String getAdvisoryRating() {
        return advisoryRating;
    }

    public String getCanonicalTitle() {
        return canonicalTitle;
    }

    public List<String> getCast() {
        return cast;
    }

    public String getGenre() {
        return genre;
    }

    public int getHasSchedules() {
        return hasSchedules;
    }

    public int getIsInactive() {
        return isInactive;
    }

    public int getIsShowing() {
        return isShowing;
    }

    public String getLinkName() {
        return linkName;
    }

    public String getPoster() {
        return poster;
    }

    public String getPosterLandscape() {
        return posterLandscape;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public long getRuntimeMinutes() {
        return runtimeMinutes;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public String getTrailer() {
        return trailer;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public int getTotalReviews() {
        return totalReviews;
    }

    public List<String> getVariants() {
        return variants;
    }

    public String getTheater() {
        return theater;
    }

    public int getOrder() {
        return order;
    }

    public int getIsFeatured() {
        return isFeatured;
    }

    public boolean isWatchList() {
        return isWatchList;
    }

    public int getYourRating() {
        return yourRating;
    }
}
