package com.gen.movieviewer.api.response;

import com.gen.movieviewer.seatbooking.SeatAvailable;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatMapResponse implements ApiResponse{

    @SerializedName("seatmap")
    private List<String[]> seatMapList;

    @SerializedName("available")
    private SeatAvailable seatAvailable;

    public List<String[]> getSeatMapList() {
        return seatMapList;
    }

    public SeatAvailable getSeatAvailable() {
        return seatAvailable;
    }
}
