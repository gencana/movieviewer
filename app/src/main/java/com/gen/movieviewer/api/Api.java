package com.gen.movieviewer.api;

import com.gen.movieviewer.api.response.MovieResponse;
import com.gen.movieviewer.api.response.ScheduleResponse;
import com.gen.movieviewer.api.response.SeatMapResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by gencana on 09/04/2018.
 */

public interface Api {

    @GET("/movie.json")
    Observable<MovieResponse> getMovie();

    @GET("/schedule.json")
    Observable<ScheduleResponse> getSchedule();

    @GET("/seatmap.json")
    Observable<SeatMapResponse> getSeatMap();
}

