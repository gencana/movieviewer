package com.gen.movieviewer.api.response;

import android.support.annotation.NonNull;

import com.gen.movieviewer.model.Cinema;
import com.gen.movieviewer.model.Cinemas;
import com.gen.movieviewer.model.MovieDates;
import com.gen.movieviewer.model.MovieTime;
import com.gen.movieviewer.model.Times;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gencana on 09/04/2018.
 */

public class ScheduleResponse implements ApiResponse{

    @SerializedName("dates")
    private List<MovieDates> movieDateList;

    @SerializedName("cinemas")
    private List<Cinemas> cinemaList;

    private List<Times> times;

    public List<MovieDates> getMovieDateList() {
        return movieDateList;
    }

    public List<Cinemas> getCinemaList() {
        return cinemaList;
    }

    public List<Times> getTimes() {
        return times;
    }

}
