package com.gen.movieviewer.api;

import javax.inject.Inject;

/**
 * Created by gencana on 09/04/2018.
 */

public class ApiHelper{
    private static ApiHelper sInstance;

    @Inject
    Api api;

    public static ApiHelper getInstance() {
        if (sInstance == null)
            sInstance = new ApiHelper();

        return sInstance;
    }

    private ApiHelper() {

    }

    public Api getApi() {
        return api;
    }
}
