package com.gen.movieviewer.data.interactor;

import com.gen.movieviewer.api.response.ApiResponse;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by gencana on 09/04/2018.
 */

public abstract class UseCase<T extends ApiResponse> {

    private CompositeDisposable compositeDisposable;

    public UseCase(){
        compositeDisposable = new CompositeDisposable();
    }

    protected abstract Observable<T> buildObservable();

    public void execute(DisposableObserver<T> observer){
        compositeDisposable.add(observer);

        buildObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void dispose(){
        compositeDisposable.dispose();
    }

}