package com.gen.movieviewer.data.interactor;

import com.gen.movieviewer.api.Api;
import com.gen.movieviewer.api.response.SeatMapResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by gencana on 09/04/2018.
 */

public class SeatAvailabilityInteractor extends UseCase<SeatMapResponse> {

    @Inject
    Api api;

    @Inject
    public SeatAvailabilityInteractor(){

    }

    @Override
    protected Observable<SeatMapResponse> buildObservable() {
        return api.getSeatMap();
    }

}
