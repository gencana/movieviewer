package com.gen.movieviewer.data.interactor;

import com.gen.movieviewer.api.Api;
import com.gen.movieviewer.api.response.ScheduleResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by gencana on 09/04/2018.
 */

public class MovieScheduleInteractor extends UseCase<ScheduleResponse> {

    @Inject
    Api api;

    @Inject
    public MovieScheduleInteractor(){

    }

    @Override
    protected Observable<ScheduleResponse> buildObservable() {
        return api.getSchedule();
    }

}
