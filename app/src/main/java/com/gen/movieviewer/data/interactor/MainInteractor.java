package com.gen.movieviewer.data.interactor;

import com.gen.movieviewer.api.Api;
import com.gen.movieviewer.api.response.MovieResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by gencana on 09/04/2018.
 */

public class MainInteractor extends UseCase<MovieResponse> {

    @Inject
    Api api;

    @Inject
    public MainInteractor(){

    }

    @Override
    protected Observable<MovieResponse> buildObservable() {
        return api.getMovie();
    }

}
