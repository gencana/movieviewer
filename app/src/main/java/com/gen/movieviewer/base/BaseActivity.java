package com.gen.movieviewer.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.gen.movieviewer.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import dagger.android.AndroidInjection;

/**
 * Created by gencana on 07/04/2018.
 */

public abstract class BaseActivity extends AppCompatActivity{

    @Nullable
    @BindView(R.id.layout_progress_bar)
    FrameLayout layoutProgressBar;

    @Nullable
    @BindView(R.id.layout_error_placeholder)
    LinearLayout layoutPlaceholder;

    @Optional
    @OnClick(R.id.btn_placeholder_retry)
    void onRetryClicked(){
        retry();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
    }

    protected abstract @LayoutRes int getLayout();

    public void showProgressBar(boolean show){
        if (layoutProgressBar != null){
            layoutProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected void retry(){

    }

    public void showErrorPlaceholder(boolean show) {
        if (layoutPlaceholder != null) {
            layoutPlaceholder.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public void showContentView(boolean show){

    }

}
