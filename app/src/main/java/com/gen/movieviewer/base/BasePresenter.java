package com.gen.movieviewer.base;

/**
 * Created by gencana on 09/04/2018.
 */

public interface BasePresenter<T extends BaseView> {

    void setView(T view);

    void onDestroy();
}
