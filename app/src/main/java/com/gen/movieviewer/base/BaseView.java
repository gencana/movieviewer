package com.gen.movieviewer.base;

/**
 * Created by gencana on 09/04/2018.
 */

public interface BaseView {

    void showProgressBar(boolean show);

    void showError(String message);

    void showErrorPlaceholder(boolean show);

}
