package com.gen.movieviewer.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import dagger.android.AndroidInjection;

public abstract class DaggerBaseActivity extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }
}
