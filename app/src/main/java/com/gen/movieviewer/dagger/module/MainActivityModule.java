package com.gen.movieviewer.dagger.module;

import com.gen.movieviewer.main.MainActivity;
import com.gen.movieviewer.main.MainContract;
import com.gen.movieviewer.data.interactor.MainInteractor;
import com.gen.movieviewer.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    MainContract.View provideMainView(MainActivity mainActivity){
        return mainActivity;
    }

    @Provides
    MainPresenter provideMainPresenter(MainContract.View mainView, MainInteractor mainInteractor){
        return new MainPresenter(mainView, mainInteractor);
    }
}
