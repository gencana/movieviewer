package com.gen.movieviewer.dagger.component;

import com.gen.movieviewer.dagger.module.MainActivityModule;
import com.gen.movieviewer.main.MainActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent(modules = MainActivityModule.class)
public interface MainActivityComponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity>{}
}