package com.gen.movieviewer.dagger.module;

import com.gen.movieviewer.api.Api;
import com.gen.movieviewer.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    Gson providesGson(){
        return new GsonBuilder().setLenient().create();
    }

    @Provides
    String providesBaseUrl() {
        return  Constants.BASE_URL;
    }

    @Provides
    GsonConverterFactory providesGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    CallAdapter.Factory providesAdapterFactory(){
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    HttpLoggingInterceptor providesHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(httpLoggingInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);
        return httpClient.build();
    }

    @Provides
    Retrofit providesRetrofit(String baseUrl,
                              GsonConverterFactory gsonConverterFactory,
                              CallAdapter.Factory callAdapterFactory,
                              OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    Api providesApi(Retrofit retrofit){
        return retrofit.create(Api.class);
    }

}
