package com.gen.movieviewer.dagger.module;

import com.gen.movieviewer.data.interactor.MovieScheduleInteractor;
import com.gen.movieviewer.data.interactor.SeatAvailabilityInteractor;
import com.gen.movieviewer.seatbooking.SeatBookingActivity;
import com.gen.movieviewer.seatbooking.SeatBookingContract;
import com.gen.movieviewer.seatbooking.SeatBookingPresenter;
import com.gen.movieviewer.seatbooking.mapper.ScheduleMapper;
import com.gen.movieviewer.seatbooking.mapper.SeatBookingMapper;

import dagger.Module;
import dagger.Provides;

@Module
public class SeatBookingModule {

    @Provides
    SeatBookingContract.View providesSeatBookingView(SeatBookingActivity activity){
        return activity;
    }

    @Provides
    ScheduleMapper providesScheduleMapper(){
        return new ScheduleMapper();
    }

    @Provides
    SeatBookingPresenter providesSeatBookingPresenter(SeatBookingContract.View view,
                                                      SeatBookingMapper mapper,
                                                      ScheduleMapper scheduleMapper,
                                                      SeatAvailabilityInteractor seatAvailabilityInteractor,
                                                      MovieScheduleInteractor movieScheduleInteractor){
        return new SeatBookingPresenter(view, mapper, scheduleMapper,  seatAvailabilityInteractor, movieScheduleInteractor);
    }

}
