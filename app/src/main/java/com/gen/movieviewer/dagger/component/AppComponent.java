package com.gen.movieviewer.dagger.component;

import android.app.Application;

import com.gen.movieviewer.MovieViewerApplication;
import com.gen.movieviewer.dagger.ActivityBuilder;
import com.gen.movieviewer.dagger.module.ApiModule;
import com.gen.movieviewer.dagger.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ApiModule.class,
        ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance Builder application(Application application);

        AppComponent build();
    }

    void inject(MovieViewerApplication app);
}