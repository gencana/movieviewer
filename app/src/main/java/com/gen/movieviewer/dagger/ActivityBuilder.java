package com.gen.movieviewer.dagger;

import com.gen.movieviewer.main.MainActivity;
import com.gen.movieviewer.dagger.module.MainActivityModule;
import com.gen.movieviewer.seatbooking.SeatBookingActivity;
import com.gen.movieviewer.dagger.module.SeatBookingModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = SeatBookingModule.class)
    abstract SeatBookingActivity bindSeatBookingActivity();

}