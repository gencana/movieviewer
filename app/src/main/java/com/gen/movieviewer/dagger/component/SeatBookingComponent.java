package com.gen.movieviewer.dagger.component;

import com.gen.movieviewer.dagger.module.SeatBookingModule;
import com.gen.movieviewer.seatbooking.SeatBookingActivity;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

@Subcomponent(modules = SeatBookingModule.class)
public interface SeatBookingComponent extends AndroidInjector<SeatBookingActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SeatBookingActivity>{}
}