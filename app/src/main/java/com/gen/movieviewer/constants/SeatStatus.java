package com.gen.movieviewer.constants;

import android.support.annotation.StringDef;

/**
 * Created by gencana on 07/04/2018.
 */

@StringDef({
        SeatStatus.AVAILABLE,
        SeatStatus.RESERVED,
        SeatStatus.SELECTED
})
public @interface SeatStatus {

    String AVAILABLE = "Available";

    String RESERVED = "Reserved";

    String SELECTED = "Selected";
}
