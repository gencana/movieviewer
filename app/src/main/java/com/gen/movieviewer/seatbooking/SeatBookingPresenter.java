package com.gen.movieviewer.seatbooking;

import com.gen.movieviewer.api.response.ScheduleResponse;
import com.gen.movieviewer.api.response.SeatMapResponse;
import com.gen.movieviewer.data.interactor.MovieScheduleInteractor;
import com.gen.movieviewer.data.interactor.SeatAvailabilityInteractor;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.seatbooking.mapper.ScheduleMapper;
import com.gen.movieviewer.seatbooking.mapper.SeatBookingMapper;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatBookingPresenter implements SeatBookingContract.Presenter{

    private SeatBookingContract.View view;

    private SeatBookingMapper mapper;

    private ScheduleMapper scheduleMapper;

    private SeatAvailabilityInteractor seatAvailabilityInteractor;

    private MovieScheduleInteractor movieScheduleInteractor;

    @Inject
    public SeatBookingPresenter(SeatBookingContract.View view,
                                SeatBookingMapper mapper,
                                ScheduleMapper scheduleMapper,
                                SeatAvailabilityInteractor seatAvailabilityInteractor,
                                MovieScheduleInteractor movieScheduleInteractor) {
        this.view = view;
        this.mapper = mapper;
        this.scheduleMapper = scheduleMapper;
        this.seatAvailabilityInteractor = seatAvailabilityInteractor;
        this.movieScheduleInteractor = movieScheduleInteractor;
    }

    @Override
    public void setView(SeatBookingContract.View view) {
        this.view = view;
    }

    @Override
    public void getMovieSchedule() {
        movieScheduleInteractor.execute(new DisposableObserver<ScheduleResponse>() {
            @Override
            public void onNext(ScheduleResponse scheduleResponse) {
                if (scheduleResponse != null){
                    scheduleMapper.setScheduleResponse(scheduleResponse);
                    view.loadMovieSchedule(scheduleMapper.getMovieDateLabelList());
                    getSeatMap();
                }
                else{
                    view.showError(null);
                }
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }

            @Override
            public void onComplete() {
            }
        });

    }

    @Override
    public List<String> getCinemaLabelList(int position) {
        return scheduleMapper.getCinemaLabelList((position));
    }

    @Override
    public List<String> getCinemaTimeLabelList(int movieDatePosition, int cinemaPosition) {
        return scheduleMapper.getTimeLabelList(movieDatePosition, cinemaPosition);
    }

    @Override
    public double getMoviePrice(int movieDatePosition, int cinemaPosition, int timePosition) {
        return scheduleMapper.getMoviePrice(movieDatePosition, cinemaPosition, timePosition);
    }

    @Override
    public void getSeatMap() {
        seatAvailabilityInteractor.execute(new DisposableObserver<SeatMapResponse>() {
            @Override
            public void onNext(SeatMapResponse seatMapResponse) {
                if (seatMapResponse != null){
                    List<List<Seat>> seatList = mapper.transformSeatMap(
                            seatMapResponse.getSeatMapList(), seatMapResponse.getSeatAvailable().getSeatList());
                    view.loadSeatMap(seatList);
                }
                else{
                    view.showError(null);
                }
            }

            @Override
            public void onError(Throwable e) {
                view.showError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });

    }

    @Override
    public void onDestroy() {
        seatAvailabilityInteractor.dispose();
        movieScheduleInteractor.dispose();
    }
}
