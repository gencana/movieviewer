package com.gen.movieviewer.seatbooking;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatAvailable {

    @SerializedName("seats")
    private List<String> seatList;

    @SerializedName("seat_count")
    private int seatCount;

    public List<String> getSeatList() {
        return seatList;
    }

    public int getSeatCount() {
        return seatCount;
    }
}
