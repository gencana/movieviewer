package com.gen.movieviewer.seatbooking;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gen.movieviewer.R;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.views.SeatRowView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatRowAdapter extends RecyclerView.Adapter<SeatRowAdapter.SeatRowViewHolder>{

    private List<List<Seat>> seatRowsList;

    private SeatRowView.SeatSelectedListener seatSelectedListener;

    private SeatRowView.SelectedSeatCounter seatCounter;

    public SeatRowAdapter(List<List<Seat>> seatRowsList,
                          SeatRowView.SeatSelectedListener seatSelectedListener,
                          SeatRowView.SelectedSeatCounter seatCounter){
        this.seatRowsList = seatRowsList;
        this.seatSelectedListener = seatSelectedListener;
        this.seatCounter = seatCounter;
    }

    @Override
    public SeatRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_row, parent, false);
        return new SeatRowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeatRowViewHolder holder, int position) {
        holder.bind(seatRowsList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return seatRowsList.size();
    }

    public class SeatRowViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.seat_row)
        SeatRowView seatRow;

        public SeatRowViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void bind(List<Seat> seatList, int position){
            seatRow.setSeatSelectedListener(seatSelectedListener);
            seatRow.setSelectedSeatCounter(seatCounter);
            seatRow.addRow(seatList, position);
        }

    }
}
