package com.gen.movieviewer.seatbooking.mapper;

import android.support.annotation.NonNull;

import com.gen.movieviewer.constants.SeatStatus;
import com.gen.movieviewer.model.Seat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatBookingMapper {

    private List<String> availableSeats;

    @Inject
    public SeatBookingMapper(){

    }

    public List<List<Seat>> transformSeatMap(@NonNull List<String[]> rawSeatList, @NonNull List<String> availableSeats){
        this.availableSeats = availableSeats;
        List<List<Seat>> rowSeatsList = new ArrayList<>();
        for (String[] seats : rawSeatList) {
            List<Seat> seatList = new ArrayList<>();
            for (String seatName : seats) {
                Seat seat = new Seat(seatName);
                if (!seatName.trim().equals(Seat.SEAT_SPACE) || !seatName.trim().equals(Seat.SEAT_SPACE_END)) {
                    seat.setStatus(isSeatAvailable(seatName) ? SeatStatus.AVAILABLE : SeatStatus.RESERVED);
                }
                seatList.add(seat);
            }
            rowSeatsList.add(seatList);
        }

        return rowSeatsList;
    }

    private boolean isSeatAvailable(@NonNull String seatName){
        for (String seat : availableSeats){
            if (seatName.equals(seat)){
                return true;
            }
        }
        return false;
    }
}
