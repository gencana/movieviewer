package com.gen.movieviewer.seatbooking;

import com.gen.movieviewer.api.response.ScheduleResponse;
import com.gen.movieviewer.base.BasePresenter;
import com.gen.movieviewer.base.BaseView;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.seatbooking.mapper.ScheduleMapper;

import java.util.List;

/**
 * Created by gencana on 07/04/2018.
 */

public interface SeatBookingContract {

    interface View extends BaseView{

        void loadMovieSchedule(List<String> movieDateList);

        void loadSeatMap(List<List<Seat>> seatRowsList);

        void resetSelection();
    }


    interface Presenter extends BasePresenter<View>{

        void getMovieSchedule();

        List<String> getCinemaLabelList(int movieDatePosition);

        List<String> getCinemaTimeLabelList(int movieDatePosition, int cinemaPosition);

        double getMoviePrice(int movieDatePosition, int cinemaPosition, int timePosition);

        void getSeatMap();
    }
}
