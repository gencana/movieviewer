package com.gen.movieviewer.seatbooking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.gen.movieviewer.R;
import com.gen.movieviewer.base.BaseActivity;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.views.SeatMapView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnItemSelected;
import dagger.android.AndroidInjection;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by gencana on 07/04/2018.
 */

public class SeatBookingActivity extends BaseActivity implements SeatBookingContract.View, SeatMapView.SeatListener {

    private static final String KEY_THEATHER_NAME = "theather_name";

    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.layout_content) LinearLayout layoutContent;

    @BindView(R.id.seat_map) SeatMapView seatMapView;

    @BindView(R.id.txt_theater) TextView txtTheater;

    @BindView(R.id.spinner_dates) Spinner spinnerDates;

    @BindView(R.id.spinner_cinemas) Spinner spinnerCinemas;

    @BindView(R.id.spinner_times) Spinner spinnerTimes;

    @Inject
    SeatBookingPresenter presenter;

    private PublishSubject<String> publishSubjectError;

    private DisposableObserver<String> disposableObserver;

    private String theatherName;

    private List<String> movieDateList;

    private ArrayAdapter<String> spinnerDateAdapter;

    private List<String> cinemaList;

    private ArrayAdapter<String> spinnerCinemaAdapter;

    private List<String> movieTimesList;

    private ArrayAdapter<String> spinnerTimesAdapter;

    private List<List<Seat>> seatList;

    public static void startActivity(Activity activity, String theatherName){
        Intent intent = new Intent(activity, SeatBookingActivity.class);
        intent.putExtra(KEY_THEATHER_NAME, theatherName);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setup();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_seat_booking;
    }

    private void setup() {
        theatherName = getIntent().getStringExtra(KEY_THEATHER_NAME);
        showContentView(false);
        showProgressBar(true);
        txtTheater.setText(theatherName);
        setupSpinners();
        setupErrorPublisher();
        seatMapView.setSeatListener(this);
        presenter.getMovieSchedule();
    }

    private void setupSpinners() {
        seatList = new ArrayList<>();
        movieTimesList = new ArrayList<>();
        spinnerTimesAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, movieTimesList);
        spinnerTimesAdapter.setDropDownViewResource(R.layout.item_dropdown_spinner);
        spinnerTimes.setAdapter(spinnerTimesAdapter);

        cinemaList = new ArrayList<>();
        spinnerCinemaAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, cinemaList);
        spinnerCinemaAdapter.setDropDownViewResource(R.layout.item_dropdown_spinner);
        spinnerCinemas.setAdapter(spinnerCinemaAdapter);

        movieDateList = new ArrayList<>();
        spinnerDateAdapter = new ArrayAdapter<>(this, R.layout.item_spinner, movieDateList);
        spinnerDateAdapter.setDropDownViewResource(R.layout.item_dropdown_spinner);
        spinnerDates.setAdapter(spinnerDateAdapter);
    }

    private void setupErrorPublisher() {
        disposableObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String s) {
                showSnackBar(s);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        publishSubjectError = PublishSubject.create();
        publishSubjectError
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribe(disposableObserver);
    }

    @Override
    public void loadMovieSchedule(List<String> movieDateLabelList) {
        movieDateList.clear();
        movieDateList.addAll(movieDateLabelList);
        spinnerDateAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadSeatMap(final List<List<Seat>> seatRowsList) {
        showProgressBar(false);
        showErrorPlaceholder(false);
        showContentView(true);
        this.seatList = seatRowsList;
        seatMapView.loadSeatMap(seatList);
    }

    @Override
    public void resetSelection() {
        seatMapView.clearAllSelection();
    }

    @Override
    public void showError(String message) {
        showProgressBar(false);
        showContentView(false);
        showSnackBar(getString(R.string.error_default_message));
        showErrorPlaceholder(true);
    }

    @Override
    public void onMaxSeatSelected() {
        publishSubjectError.onNext(getString(R.string.error_max_seats_selected));
    }

    @Override
    public void onEmptyMovie() {
        publishSubjectError.onNext(getString(R.string.error_no_movie));
    }


    private void showSnackBar(String message){
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    @Override
    protected void onDestroy() {
        disposableObserver.dispose();
        presenter.onDestroy();
        super.onDestroy();
    }

    @OnItemSelected(R.id.spinner_dates)
    void onSpinnerDateSelected(AdapterView<?> parent, View view, int position, long id){
        if (position != AdapterView.NO_ID && cinemaList != null && spinnerCinemaAdapter != null){
            cinemaList.clear();
            cinemaList.addAll(presenter.getCinemaLabelList(position));
            if (cinemaList.size() > 0) {
                spinnerCinemas.setSelection(0);
            }
            spinnerCinemaAdapter.notifyDataSetChanged();
            resetSelection();
        }
    }

    @OnItemSelected(value = R.id.spinner_dates, callback =  OnItemSelected.Callback.NOTHING_SELECTED)
    void onSpinnerDateNothingSelected(){
        if (movieDateList.size() == 0){
            cinemaList.clear();
            spinnerCinemaAdapter.notifyDataSetChanged();
            resetSelection();
        }
    }

    @OnItemSelected(R.id.spinner_cinemas)
    void onSpinnerCinemaSelected(AdapterView<?> parent, View view, int position, long id){
        if (position != AdapterView.NO_ID && movieTimesList != null && spinnerTimesAdapter != null){
            movieTimesList.clear();
            movieTimesList.addAll(presenter.getCinemaTimeLabelList(spinnerDates.getSelectedItemPosition(), position));
            if (movieTimesList.size() > 0) {
                spinnerTimes.setSelection(0);
            }
            spinnerTimesAdapter.notifyDataSetChanged();

            seatMapView.setPrice(0);
            resetSelection();
        }
    }

    @OnItemSelected(value = R.id.spinner_cinemas, callback =  OnItemSelected.Callback.NOTHING_SELECTED)
    void onSpinnerCinemaNothingSelected(){
            if (cinemaList.size() == 0){
                movieTimesList.clear();
                spinnerTimesAdapter.notifyDataSetChanged();
                seatMapView.setPrice(0);
                resetSelection();
            }
    }

    @OnItemSelected(R.id.spinner_times)
    void onSpinnerTimesSelected(AdapterView<?> parent, View view, int position, long id){
        if (position != AdapterView.NO_ID && movieTimesList != null){
            seatMapView.setHasSelectedMovie(true);
            seatMapView.setPrice(presenter.getMoviePrice(spinnerDates.getSelectedItemPosition(),
                    spinnerCinemas.getSelectedItemPosition(), position));
            resetSelection();
        }
    }

    @OnItemSelected(value = R.id.spinner_times, callback =  OnItemSelected.Callback.NOTHING_SELECTED)
    void onSpinnerTimesNothingSelected(){
        if (movieTimesList.size() == 0) {
            seatMapView.setHasSelectedMovie(false);
            resetSelection();
        }
    }

    @Override
    protected void retry() {
        presenter.getMovieSchedule();
    }

    @Override
    public void showContentView(boolean show){
        layoutContent.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
