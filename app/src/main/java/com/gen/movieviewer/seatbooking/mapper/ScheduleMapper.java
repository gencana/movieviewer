package com.gen.movieviewer.seatbooking.mapper;

import com.gen.movieviewer.api.response.ScheduleResponse;
import com.gen.movieviewer.model.Cinemas;
import com.gen.movieviewer.model.MovieDates;
import com.gen.movieviewer.model.MovieTime;
import com.gen.movieviewer.model.Times;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gencana on 07/04/2018.
 */

public class ScheduleMapper {

    private ScheduleResponse scheduleResponse;

    public void setScheduleResponse(ScheduleResponse scheduleResponse){
        this.scheduleResponse = scheduleResponse;
    }

    public List<String> getMovieDateLabelList(){
        List<String> movieDateLabelList = new ArrayList<>();
        if (scheduleResponse != null && scheduleResponse.getMovieDateList() != null){
            for (MovieDates movieDate : scheduleResponse.getMovieDateList()){
                movieDateLabelList.add(movieDate.getLabel());
            }
        }
        return movieDateLabelList;
    }

    public List<Cinemas> getCinemaList(int movieDatePosition){
        List<Cinemas> list = new ArrayList<>();
        MovieDates movieDates = getMovieDate(movieDatePosition);
        if (movieDates == null){
            return list;
        }

        for (Cinemas cinemas : scheduleResponse.getCinemaList()){
            if (cinemas.getParent() != null && cinemas.getParent().equals(movieDates.getId())){
                if (cinemas.getCinemaList() != null) {
                    for (Cinemas cinemaDetail : cinemas.getCinemaList()) {
                        list.add(cinemaDetail);
                    }
                }

            }
        }
        return list;
    }

    public List<String> getCinemaLabelList(int movieDatePosition){
        List<String> list = new ArrayList<>();
        MovieDates movieDates = getMovieDate(movieDatePosition);
        if (movieDates == null){
            return list;
        }

        for (Cinemas cinemas : scheduleResponse.getCinemaList()){
            if (cinemas.getParent() != null && cinemas.getParent().equals(movieDates.getId())){
                if (cinemas.getCinemaList() != null) {
                    for (Cinemas cinemaDetail : cinemas.getCinemaList()) {
                        list.add(cinemaDetail.getLabel());
                    }
                }

            }
        }
        return list;
    }

    private MovieDates getMovieDate(int movieDatePosition){
        return movieDatePosition != -1 && scheduleResponse != null &&
                scheduleResponse.getMovieDateList() != null &&
                movieDatePosition < scheduleResponse.getMovieDateList().size() ?
                scheduleResponse.getMovieDateList().get(movieDatePosition) : null;
    }

    public List<MovieTime> getTimeList(int movieDatePosition, int cinemaPosition){
        List<MovieTime> list = new ArrayList<>();
        List<Cinemas> cinemasList = getCinemaList(movieDatePosition);
        if (cinemaPosition >= 0 && cinemasList.size() > cinemaPosition){
            String parent = cinemasList.get(cinemaPosition).getId();
            for (Times times : scheduleResponse.getTimes()){
                if (times.getParent() != null && times.getParent().equals(parent)){
                    if (times.getTimeList() != null) {
                        for (MovieTime timeDetail : times.getTimeList()) {
                            list.add(timeDetail);
                        }
                    }

                }
            }
        }

        return list;
    }

    public List<String> getTimeLabelList(int movieDatePosition, int cinemaPosition){
        List<String> list = new ArrayList<>();
        List<Cinemas> cinemasList = getCinemaList(movieDatePosition);
        if (cinemaPosition >= 0 && cinemasList.size() > cinemaPosition){
            String parent = cinemasList.get(cinemaPosition).getId();
            for (Times times : scheduleResponse.getTimes()){
                if (times.getParent() != null && times.getParent().equals(parent)){
                    if (times.getTimeList() != null) {
                        for (MovieTime timeDetail : times.getTimeList()) {
                            list.add(timeDetail.getLabel());
                        }
                    }

                }
            }
        }

        return list;
    }

    public double getMoviePrice(int movieDatePosition, int cinemaPosition, int timePosition){
        List<MovieTime> list = getTimeList(movieDatePosition, cinemaPosition);
        if (timePosition >= 0 && list.size() > timePosition){
            return list.get(timePosition).getPrice();
        }

        return 0;
    }

}
