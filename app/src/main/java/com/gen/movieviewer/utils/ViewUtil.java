package com.gen.movieviewer.utils;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;

/**
 * Created by gencana on 15/04/2018.
 */

public class ViewUtil {

    public static int getScreenWidth(Activity context){
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

}
