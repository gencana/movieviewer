package com.gen.movieviewer.utils;

import android.content.Context;

import com.gen.movieviewer.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gencana on 09/04/2018.
 */

public class FormatUtil {

    private static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd";

    private static final String DATE_READABLE_FORMAT = "MMM d, yyyy";

    public static String formatNumberWithCurrency(Context context, double amount){
        return String.format("%s %,.2f", context.getString(R.string.currency), amount);
    }

    public static String formatDuration(long duration) {
        if (duration <= 0){
            return "";
        }

        int hour = (int)duration/60;
        int minutes = (int) duration % 60;

        StringBuilder durationFormat = new StringBuilder();
        if (hour > 0){
            durationFormat.append(String.format("%d %s ", hour, hour == 1 ? "hr" : "hrs"));
        }

        if (minutes > 0){
            durationFormat.append(String.format("%d %s ", minutes, minutes == 1 ? "min" : "mins"));
        }

        return durationFormat.toString();
    }

    public static String formatDate(String date){
        return formateDateFromstring(DATE_DEFAULT_FORMAT, DATE_READABLE_FORMAT, date);
    }

    private static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }
}
