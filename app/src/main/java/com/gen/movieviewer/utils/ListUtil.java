package com.gen.movieviewer.utils;

import com.gen.movieviewer.model.Cinemas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gencana on 09/04/2018.
 */

public class ListUtil {

    public static List<String> getCinemaLabelList(List<Cinemas> cinemaList){
        List<String> list = new ArrayList<>();
        for (Cinemas cinemas : cinemaList){
            list.add(cinemas.getLabel());
        }

        return list;
    }
}
