package com.gen.movieviewer.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.gen.movieviewer.R;
import com.gen.movieviewer.constants.SeatStatus;
import com.gen.movieviewer.model.Seat;
import com.gen.movieviewer.views.SquareBox;
import com.gen.movieviewer.views.AutoSizeTextView;

/**
 * Created by gencana on 07/04/2018.
 */

public class ViewGenerator {

    public static AutoSizeTextView addRowLabel(Context context, String label, int size){
        AutoSizeTextView view = (AutoSizeTextView)LayoutInflater.from(context)
                .inflate(R.layout.item_row_label, null, true);

        view.setPadding(0, 0, 0, 0);
        view.setLayoutParams(getDefaultLayoutParams(size));
        view.setGravity(Gravity.CENTER_HORIZONTAL);
        view.setText(label);
        return view;
    }

    public static SquareBox addSeatView(Context context, Seat seat, int size,
                                        CompoundButton.OnCheckedChangeListener checkListener,
                                        View.OnTouchListener touchListener){
        SquareBox squareBox = new SquareBox(context);
        squareBox.setLayoutParams(getDefaultLayoutParams(size));
        squareBox.setPadding(0, 0, 0, 0);
        squareBox.setTag(seat.getName());
        squareBox.setOnCheckedChangeListener(checkListener);
        squareBox.setChecked(false);
        if (seat.getName().equals(Seat.SEAT_SPACE)){
            squareBox.setEnabled(false);

        }
        else if (seat.getName().equals(Seat.SEAT_SPACE_END)){
            squareBox.setEnabled(false);

        }
        else{
            switch (seat.getStatus()){
                case SeatStatus.RESERVED:
                    squareBox.setActivated(true);
                    break;
                default:
                    squareBox.setOnTouchListener(touchListener);
                    break;
            }

        }
        return squareBox;
    }

    private static LinearLayout.LayoutParams getDefaultLayoutParams(int size){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.setMargins(2, 0, 0, 0);
        return layoutParams;
    }

}
