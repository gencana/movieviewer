package com.gen.movieviewer.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gencana on 09/04/2018.
 */

public class Cinemas {

    private String parent;

    @SerializedName("cinemas")
    private List<Cinemas> cinemaList;

    private String id;

    @SerializedName("cinema_id")
    private String cinemaId;

    private String label;

    public String getId() {
        return id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public String getLabel() {
        return label;
    }

    public String getParent() {
        return parent;
    }

    public List<Cinemas> getCinemaList() {
        return cinemaList;
    }

}
