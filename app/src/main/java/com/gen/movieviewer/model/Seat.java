package com.gen.movieviewer.model;

import com.gen.movieviewer.constants.SeatStatus;

/**
 * Created by gencana on 07/04/2018.
 */

public class Seat {

    public static final String SEAT_SPACE = "b(20)";

    public static final String SEAT_SPACE_END = "a(30)";

    private String row;

    private String name;

    private @SeatStatus String status;

    public Seat(String name) {
        this.name = name;
        setRow();
    }

    public Seat(String name, String status) {
        this.name = name;
        this.status = status;
        setRow();
    }

    public void setName(String name) {
        this.name = name;
        setRow();
    }

    public void setStatus(@SeatStatus String status) {
        this.status = status;
    }

    public String getRow() {
        return row;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    private void setRow(){
        if (name != null && name.length() > 0){
            row = String.valueOf(name.charAt(0));
        }
    }
}
