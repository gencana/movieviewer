package com.gen.movieviewer.model;

/**
 * Created by gencana on 09/04/2018.
 */

public class MovieDates {

    private String id;

    private String label;

    private String date;

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDate() {
        return date;
    }
}
