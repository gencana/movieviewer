package com.gen.movieviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gencana on 09/04/2018.
 */

public class Cinema {

    private String id;

    @SerializedName("cinema_id")
    private String cinemaId;

    private String label;

    public String getId() {
        return id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public String getLabel() {
        return label;
    }
}
