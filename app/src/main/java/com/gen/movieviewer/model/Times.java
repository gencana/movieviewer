package com.gen.movieviewer.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gencana on 09/04/2018.
 */

public class Times {

    private String parent;


    @SerializedName("times")
    private List<MovieTime> timeList;

    public String getParent() {
        return parent;
    }

    public List<MovieTime> getTimeList() {
        return timeList;
    }
}