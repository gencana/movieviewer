package com.gen.movieviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gencana on 09/04/2018.
 */

public class MovieTime {

    private String id;

    private String label;

    @SerializedName("schedule_id")
    private String scheduleId;

    @SerializedName("popcorn_price")
    private double popcornPrice;

    @SerializedName("popcorn_label")
    private String popcornLabel;

    @SerializedName("seating_type")
    private String seatingType;

    private double price;

    private String variant;

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public double getPopcornPrice() {
        return popcornPrice;
    }

    public String getPopcornLabel() {
        return popcornLabel;
    }

    public String getSeatingType() {
        return seatingType;
    }

    public double getPrice() {
        return price;
    }

    public String getVariant() {
        return variant;
    }
}
